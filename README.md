## Description
Gender prediction. It is based on spark-ml module and basically uses LogisticRegression, TF-IDF-vectorizer, data-preprocessing.

## Repository composition
 * data - data for dictionaries, jsons, etc
 * core - data pipelines, utils, decorators, loaders
 * notebooks - is for experiments

## Context params
For launching main.py just use bash example_context.sh in spark-submit mode.

example_context.sh has params:

zip -qr pyspark.zip core

/bin/spark2-submit \
	--files core.yaml \
	--py-files pyspark.zip \
	--conf spark.yarn.queue='root.g_dl_u_corp.korniltsev1-da_ca-sbrf-ru' \
	--conf spark.dynamicAllocation.enabled='true' \
	--conf spark.dynamicAllocation.maxExecutors='80' \
	--conf spark.dynamicAllocation.minExecutors='1' \
	--conf spark.executor.instances='20' \
	--conf spark.yarn.executor.memoryOverhead='10g' \
	--conf spark.executor.cores='2' \
	--conf spark.executor.memory='10g' \
	--conf spark.driver.memory='30g' \
	--conf spark.driver.maxResultSize='16g' \
	--conf spark.yarn.driver.memoryOverhead='5g' \
	--conf spark.sql.shuffle.partitions='800' \
	--conf spark.default.parallelism='800' \
	--conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
	--conf spark.port.maxRetries=200 \
	main.py \
    node_mode=fit_predict