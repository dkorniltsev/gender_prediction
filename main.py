# -*- coding: utf-8 -*-
import datetime
from dateutil.relativedelta import *
import pandas as pd
import os
import sys
from pyspark.sql.window import *

from core.model import Model
from core.load_config import MainConfig
from logging import get_logger
from pyspark import SparkContext, SparkConf, HiveContext

logger = get_logger()

SparkAppName = 'gender_prediction'
spark_home = '/opt/spark22/SPARK2-2.2.0.cloudera2-1.cdh5.12.0.p0.232957/lib/spark2'
os.environ['SPARK_HOME'] = spark_home
os.environ['PYSPARK_DRIVER_PYTHON'] = 'python'
os.environ['PYSPARK_PYTHON'] = '/opt/cloudera/parcels/PYENV.ZNO52292115/bin/python'
sys.path.insert(0, os.path.join (spark_home,'python'))
sys.path.insert(0, os.path.join (spark_home,'python/lib/py4j-0.10.4-src.zip'))

spark_conf = SparkConf() \
    .setAppName(SparkAppName.format(str(dt.now()).split()[1][:5])) \
    .setMaster("yarn-client") \
    .set('spark.yarn.queue', 'root.g_dl_u_corp.biryukov2-nd_ca-sbrf-ru') \
    .set('spark.dynamicAllocation.enabled', 'true') \
    .set('spark.dynamicAllocation.maxExecutors', '80') \
    .set('spark.executor.cores', '2') \
    .set('spark.driver.maxResultSize', '8g') \
    .set('spark.executor.memory','15g') \
    .set('spark.driver.memory', '30g') \
    .set('spark.sql.shuffle.partitions', '800') \
    .set('spark.default.parallelism', '800') \
    .set('spark.port.maxRetries','150')

class Main():
    def run(self):

        config = MainConfig('./config.yaml')
        logger.info('Start')
        sc = SparkContext.getOrCreate(conf=spark_conf)
        hive = HiveContext(sc)
        logger.info('Allocated')

        model = Model(sc, hive, config)

        self.conf['context_id'] = datetime.datetime.isoformat(datetime.datetime.now())

        if self.conf['node_mode'] == 'train':
            logger.info("Start train")
            model.train()
            logger.info("Finish train")

        elif self.conf['node_mode'] == 'predict':
            logger.info("Start predict")
            model.predict()
            logger.info("Finish predict")

        elif self.conf['node_mode'] == 'train_test_validation':
            logger.info("Start train_test_validation")
            model.train_test_validation()
            logger.info("Finish predict")

        elif self.conf['node_mode'] == 'fit_predict':
            logger.info("Start fit_predict")
            model.fit_predict()
            logger.info("Finish fit_predict")

if __name__ == '__main__':
    Main()
