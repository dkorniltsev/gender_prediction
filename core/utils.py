import numpy as np
import pandas as pd
from pyspark.sql.types import StructField, StringType, DoubleType, LongType, \
    StructType
import pyspark.sql.functions as F

def get_replaced_main_field_from_replaced_unique_key(df, hive, path_files, replaced_unique_key='replaced_unique_key'):
    """
    создание матрицы признаков вх/исх и их отнощение к общему числу
    транзакций
    """
    @F.udf('string')
    def _replaced_unique_key_replaced_main_field(replaced_unique_key):

        try:
            test_int = int(replaced_unique_key)
            replaced_main_field = replaced_unique_key[:2]
            if replaced_main_field in replaced_unique_key_to_replaced_main_field_dict:
                return replaced_unique_key_to_replaced_main_field_dict[replaced_main_field]
            else:
                return '999'
        except ValueError:
            return '999'

    # load dictionary replaced_unique_key mask : replaced_main_field

    data_path = '/oozie-app/cb/_models/okk/_TURNOVER/pyspark/data/'
    replaced_unique_key_to_replaced_main_field_dict = hive.read.csv(path_files,
                                      header=True) \
        .toPandas() \
        .drop_duplicates()[['REGION_ID', 'replaced_main_field']] \
        .dropna()

    replaced_unique_key_to_replaced_main_field_dict = dict(
        zip(replaced_unique_key_to_replaced_main_field_dict['REGION_ID'], replaced_unique_key_to_replaced_main_field_dict['replaced_main_field']))

    for i in ['00', '91', '92']:
        del replaced_unique_key_to_replaced_main_field_dict[i]

    df = df.withColumn('replaced_main_field', _replaced_unique_key_replaced_main_field(F.col(replaced_unique_key)))
    return df

def get_top_n_classes(proba, model, top_n=3):
    # get indexes for top_n probabilities
    # top_n = int(top_n)
    top_n_pred = np.argsort(-proba, axis=1)[:, : top_n]
    class_labels = model.classes_
    # get class labels by indexes (top_n_pred)
    labels_df = class_labels[top_n_pred]
    labels_df = pd.DataFrame(labels_df)
    # get probabilities by indexes (top_n_pred)
    proba_df = np.sort(-proba, axis=1)[:, : top_n] * (-1)
    proba_df = pd.DataFrame(proba_df)

    def create_header():
        col_num = top_n - 1
        while col_num >= 0:
            labels_df.rename(columns={col_num: 'class_top_{}'.format(str(col_num + 1))}, inplace=True)
            proba_df.rename(columns={col_num: 'proba_top_{}'.format(str(col_num + 1))}, inplace=True)
            col_num -= 1
            result = pd.concat([labels_df, proba_df], axis=1)
        return result

    return create_header()

def to_spark(df, hive):

    dtype_ser = df.dtypes

    schema_list = []

    for col in zip(dtype_ser.index, dtype_ser):
        if 'O' in col:
            schema_list.append(StructField(col[0], StringType()))
        elif 'float' in col:
            schema_list.append(StructField(col[0], DoubleType()))
        elif 'int' in col:
            schema_list.append(StructField(col[0], LongType()))

    schema = StructType(schema_list)

    df_spark = hive.createDataFrame(data=df, schema=schema)

    return df_spark


def _fill_replaced_unique_key(replaced_unique_key):
    if len(str(replaced_unique_key)) in (9, 11):
        return '0' + str(replaced_unique_key)
    else:
        return str(replaced_unique_key)

def bucket_iterator(iterator: iter, size: int):
    """
    Get part from RDD iterator to prevent OverHeadMemory
    :param iterator:
    :param size: size of bucket
    :return: bucket with fixed size of rows
    """
    bucket = list()
    for e in iterator:
        bucket.append(e)
        if len(bucket) > size:
            yield bucket
            bucket = list()
    if bucket:
        yield bucket

def to_pandas(df, n_partitions=None):

    def _map_to_pandas(rdds, n_partitions=None):
        if n_partitions is None:
            for one_rdd in bucket_iterator(rdds, 5000):
                yield pd.DataFrame(list(one_rdd))
        else:
            return [pd.DataFrame(list(rdds))]

    if n_partitions is not None:
        df = df.repartition(n_partitions)
    df_pand = df.rdd.mapPartitions(lambda x: _map_to_pandas(x, n_partitions)).collect()
    df_pand = pd.concat(df_pand)
    df_pand.columns = df.columns
    return df_pand