import os
import yaml

class MainConfig(object):
    def __init__(self, filepath):

        self.filepath = filepath
        self.load()

    def load(self):

        if os.path.isfile(self.filepath):
            with open(self.filepath) as f:
                data = yaml.load(f)
            f.close()
            self.__dict__ = data
        else:
            raise Exception('file {} does not exist.'.format(self.filepath))