import re
import pandas as pd
from pyspark.sql.window import *
import pyspark.sql.functions as F
from core.utils import
from core.pipeline import Executor

class TextTokenizer(Executor):
    def __init__(self, sc, hive, config):
        super().__init__(sc, hive)

        self.config = config
        self.schema = config.SCHEMA
        self.table = config.TABLES['T_NAME']

    def preprocess(self, df):
        steps = []
        # tokenizer =
        # stemmer =
        # steps += tokenizer
        # steps += stemmer
        pass

    def evaluate(self, *args):
        df = self.hive.table(f"{self.schema}.{self.table}")
        df = self.preprocess(df)

        return df

class TfIdfVectorizer(Executor):

    def __init__(self, sc, hive, config):
        super().__init__(sc, hive)

        self.config = config
        self.schema = config.SCHEMA
        self.table = config.TABLES['T_NAME']

    def preprocess(self, df):
        pass

    def evaluate(self, *args):

        df = self.hive.table(f"{self.schema}.{self.table}")
        df = self.preprocess(df)

        return df