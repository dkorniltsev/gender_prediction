# clases Users, Feature_1, Feature_2, Feature_3, Feature_4, Feature_5, Feature_6 are given to show conception
from core.features import TextTokenizer, TfIdfVectorizer, Users, Feature_1, Feature_2, Feature_3, Feature_4, Feature_5, Feature_6
from core.pipeline import Executor

class ModelTrainExecutor(Executor):

    def __init__(self, sc, hive, config):
        super().__init__(sc, hive)
        self.sc = sc
        self.hive = hive
        self.config = config

    def evaluate(self, *args):

        df = Users(self.sc, self.hive, self.config).evaluate()\
            .join(Feature_1(self.sc, self.hive, self.config).evaluate()\
            .join(Feature_2(self.sc, self.hive, self.config).evaluate()
                                                                                                            
        return df


class ModelPredictExecutor(Executor):

    def __init__(self, sc, hive, config):
        super().__init__(sc, hive)
        self.sc = sc
        self.hive = hive
        self.config = config


    def evaluate(self, *args):

        df = Users(self.sc, self.hive, self.config).evaluate()\
            .join(Feature_1(self.sc, self.hive, self.config).evaluate()\
            .join(Feature_2(self.sc, self.hive, self.config).evaluate()

        return df