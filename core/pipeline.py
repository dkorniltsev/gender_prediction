from abc import ABC, abstractmethod


class Executor(ABC):
    """
    Base class for spark dataframes creation 
    """

    def __init__(self, sc, hive):
        self.sc = sc
        self.hive = hive
        super().__init__()

    @abstractmethod
    def evaluate(self, *args):
        pass

    def get_default_part_num(self):
        part_num = self.sc._conf.get('spark.default.parallelism')
        if not part_num:
            part_num = '1000'
        return int(part_num)
