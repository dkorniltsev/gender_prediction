import json
import subprocess
import os
import gc
import re
import sys
from core.execution_combiner import ModelTrainExecutor, ModelPredictExecutor

import pyspark.sql.functions as F
from pyspark.sql.types import DoubleType, DateType, StringType
from pyspark.ml.feature import OneHotEncoderEstimator, StringIndexer, VectorAssembler, StandardScaler
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator
from pyspark.ml import Pipeline

SEED = 42

class Model:

    def __init__(self, sc, hive, config, country):
        self.sc = sc
        self.hive = hive
        self.config = config
        self.country = country
        self.path_to_model = config.PATH_TO_MODEL

    @staticmethod
    def save_model(model, path):
        os.mkdir(path)
        # ! mkdir -p ./path_to_model/model_name
        model.save(path, 'model_name')
        print(f'path to model: {path}')

    @staticmethod
    def load_model(path):
        print(f'path to model: {path}')
        model = LogisticRegression.load(path, 'model_name')
        return model

    @staticmethod
    def preprocess(df):
        stages = []
        scaler = StandardScaler(inputCol="vectorized_features", outputCol="features")
        stages += [scaler]
        pipeline = Pipeline(stages=stages)
        pipelineModel = pipeline.fit(df)
        df = pipelineModel.transform(df)
        return df

    def train_test_validation(self, df):
        df = ModelTrainExecutor(self.sc, self.hive, self.config).evaluate()
        train, test = df.randomSplit([0.8, 0.2], seed=SEED)
        lr = LogisticRegression(featuresCol='features', labelCol='label', maxIter=5)
        lrModel = lr.fit(train)
        predictions = lrModel.transform(test)
        evaluator = BinaryClassificationEvaluator()
        print('ROC AUC', evaluator.evaluate(predictions))
        return evaluator

    def cv_hyper_param_tuning(self, df, save_best_model=False, path_to_model=None):
        df = self.preprocess(df)
        train, test = df.randomSplit([0.8, 0.2], seed=SEED)
        lr = LogisticRegression(featuresCol='features', labelCol='label', maxIter=5)
        evaluator = BinaryClassificationEvaluator()
        paramGrid = (ParamGridBuilder()
                     .addGrid(lr.regParam, [0.01, 0.5, 2.0])  # regularization parameter
                     .addGrid(lr.elasticNetParam, [0.0, 0.5, 1.0])  # Elastic Net Parameter (Ridge = 0)
                     .addGrid(lr.maxIter, [1, 5, 10])  # Number of iterations
                     .build())

        cv = CrossValidator(estimator=lr, estimatorParamMaps=paramGrid,
                            evaluator=evaluator, numFolds=5)

        cvModel = cv.fit(train)
        # Evaluate Best Model
        predictions = cvModel.transform(test)

        print('Best Model Test Area Under ROC', evaluator.evaluate(predictions))

        if save_best_model:
            cvModel.bestModel.write().overwrite().save(self.path_to_model)
        return evaluator, cvModel

    def train(self):
        self.logger.info('Start train model')
        df = ModelTrainExecutor(self.sc, self.hive, self.config).evaluate()

        lr = LogisticRegression(featuresCol='features', labelCol='label', maxIter=5)
        lrModel = lr.fit(df)

        self.save_model(lrModel, self.path_to_model)
        return lrModel

    def predict(self):
        predict = ModelPredictExecutor(self.sc, self.hive, self.config).evaluate()
        lrModel = self.load_model(self.path_to_model)
        predictions = lrModel.transform(predict)
        return predictions

    def fit_predict(self):
        train = ModelTrainExecutor(self.sc, self.hive, self.config).evaluate()
        predict = ModelPredictExecutor(self.sc, self.hive, self.config).evaluate()

        lr = LogisticRegression(featuresCol='features', labelCol='label', maxIter=5)
        lrModel = lr.fit(train)
        predictions = lrModel.transform(predict)
        return predictions


